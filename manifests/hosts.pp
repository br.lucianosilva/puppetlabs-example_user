class example_user::hosts {
file {'hosts':
	ensure => file,
        owner => 'root',
        group => 'root',
        mode => '0644',
	path => '/etc/hosts',
        source => 'puppet:///modules/example_user/hosts',
	}
}
