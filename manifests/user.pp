
class example_user::user {
group {"sysadmin":
        ensure => "present",
        gid => "5000",
        }

$array_user=['administrator','brascloud']
user {$array_user:
        ensure => "present",
        gid => "sysadmin",
        shell => "/bin/bash",
        managehome => "true",
	require => Group['sysadmin'],
        }

file {'sudoers':
        ensure => file,
        owner => 'root',
        group => 'root',
        mode => '0440',
        path => '/etc/sudoers',
        source => 'puppet:///modules/example_user/sudoers',
        }

}
